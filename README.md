# Task project
This is a sample task project for coding interview.

## Getting started
Get the API key from `https://alphavantage.co/` and  create a search form where we can search companies. Then, if we click the company name, we will be able to see the current stock price and a table of historical data for 100 days.


